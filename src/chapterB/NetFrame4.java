package chapterB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//B-10
public class NetFrame4 extends JFrame {
    JTextField tf = new JTextField(10);
    JButton b = new JButton("連 線");
    public NetFrame4() {
        Container c = getContentPane();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300, 150);
        //17-21行 於addActionListener方法內設計一匿名類別 將傾聽者物件listener註冊至b按鈕
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("匿名:Action事件發生");
            }
        });
        Panel p = new Panel();
        p.add(tf);
        p.add(b);
        c.add(p, BorderLayout.NORTH);
        setVisible(true);
    }

    public static void main(String[] args){
        new NetFrame4();
    }

}
