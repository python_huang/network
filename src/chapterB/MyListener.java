package chapterB;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//B-4
//要設計一個具有ActionListener功能的傾聽者MyListener類別 則要繼承(實作)ActionListener介面
//並實作它所規範的actionPerformed方法
public class MyListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("Action事件發生");
    }
}
