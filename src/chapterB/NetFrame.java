package chapterB;

import javax.swing.*;
import java.awt.*;

//B-5
public class NetFrame extends JFrame {
    JTextField tf = new JTextField(10);
    JButton b = new JButton("連 線");
    //產生自行設計的MyListener物件
    MyListener listener = new MyListener();
    public NetFrame() {
        Container c = getContentPane();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300,150);
        //將傾聽者物件listener註冊至b按鈕
        b.addActionListener(listener);
        Panel p = new Panel();
        p.add(tf);
        p.add(b);
        c.add(p, BorderLayout.NORTH);
        setVisible(true);
    }
    public static void main(String[] args) {
        new NetFrame();
    }
}
//外部類別
//圖形介面類別與傾聽者類別是分開的