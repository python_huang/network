package chapterB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//B-9
//NetFrame3類別實作ActionListener
public class NetFrame3 extends JFrame implements ActionListener {
    JTextField tf = new JTextField(10);
    JButton b = new JButton("連 線");
    public NetFrame3() {
        Container c = getContentPane();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300, 150);
        //將傾聽者物件listener註冊至b按鈕
        //由於NetFrame3類別即是ActionListener傾聽者 因此以this關鍵字註冊本身傾聽者物件即可
        b.addActionListener(this);
        Panel p = new Panel();
        p.add(tf);
        p.add(b);
        c.add(p, BorderLayout.NORTH);
        setVisible(true);
    }
    //由於實作了ActionListener 故必須一併實作介面中定義的方法actionPerformed
    public void actionPerformed(ActionEvent e) {
        System.out.println("自己:Action事件發生");
    }

    public static void main(String[] args) {
        new NetFrame3();
    }
}
//自行實作傾聽者介面省去額外產生一個傾聽者物件