package chapterB;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//B-7
public class NetFrame2 extends JFrame {
    JTextField tf = new JTextField(10);
    JButton b = new JButton("連 線");
    //產生自行設計的InnerListener物件
    InnerListener listener = new InnerListener();
    public NetFrame2() {
        Container c = getContentPane();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300,150);
        //將傾聽者物件listener註冊至b按鈕
        b.addActionListener(listener);
        Panel p = new Panel();
        p.add(tf);
        p.add(b);
        c.add(p, BorderLayout.NORTH);
        setVisible(true);
    }
    //26-30行 內部類別InnerListener
    class InnerListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.out.println("Inner:Action事件發生");
        }
    }

    public static void main(String[] args) {
        new NetFrame2();
    }
}
//在內部類別中仍可存取圖形介面內的屬性 方法