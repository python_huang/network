package chapter12;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

//12-9 12-15
//定義FtpHandler類別 並繼承Thread類別
public class FtpHandler extends Thread {
    //16-18行 為FtpHandler類別訂定三個屬性
    BufferedReader in = null;
    PrintWriter out = null;
    boolean logon = false;
    Map<String, Integer> map;
    public static final int USER = 1;
    public static final int PASS = 2;
    public static final int PWD = 3;
    public static final int LIST = 4;
    public static final int TYPE = 5;
    public static final int PASV = 6;
    public static final int RETR = 7;
    public static final int STOR = 8;
    String directory = "/";
    Socket dataSocket = null;

    //類別的建構子 接收一個Socket物件s為參數 得到此通道的輸入與輸出串流物件in與out
    public FtpHandler(Socket s) {
        try {
            in = new BufferedReader(new InputStreamReader(
                    s.getInputStream()));
            out = new PrintWriter(s.getOutputStream());
        } catch (IOException e) {
            System.out.println("資料串流輸出入錯誤");
        }
        map = new HashMap<String, Integer>();
        map.put("USER", USER);
        map.put("PASS", PASS);
        map.put("PWD", PWD);
        map.put("LIST", LIST);
        map.put("TYPE", TYPE);
        map.put("PASV", PASV);
        map.put("RETR", RETR);
        map.put("STOR", STOR);
    }

    //覆寫Thread類別的方法run()
    public void run() {
        reply("220", "Welcome to My FTP Server");
        String line = "";
        try {
            while ((line = in.readLine()) != null) {
                StringTokenizer stk = new StringTokenizer(line);
                String cmd = stk.nextToken();
                System.out.println(line);
                int cmdInt = map.get(cmd) == null ? -1 : map.get(cmd);
                switch (cmdInt) {
                    //用戶端使用USER指令要求伺服器為其進行驗證 USER指令後應提供登入的帳號密碼
                    //伺服器應回應331碼 告知使用者該命令已被接収 等待下一個指令完成整個登入流程
                    case USER:
                        reply("331", "Please specify the password.");
                        break;
                    //用戶端傳送密碼與伺服器 通常在用戶端使用USER指令傳送帳號後 應再傳送該帳號的密碼
                    //供伺服器進行帳號驗證
                    case PASS:
                        reply("230", "Login successful.");
                        break;
                    //要求伺服器回報目前用戶在哪個工作目錄 伺服器以257回應目錄字串
                    case PWD:
                        reply("257", directory);
                        break;
                    case TYPE:
                        String mode = stk.nextToken();
                        if (mode.equals("I"))
                            reply("200", "Switching to Binary mode.");
                        if (mode.equals("A"))
                            reply("200", "Switch to ASCII mode.");
                        break;
                    //要求伺服器將用戶端目前所在目錄下的檔案清單 以預先建立的資料傳輸通道傳輸
                    case LIST:
                        reply("150", "Here comes the directory listing.");
                        PrintWriter dataOut = new PrintWriter(
                                dataSocket.getOutputStream());
                        File currentDir = new File("C:\\net\\" + directory);
                        File[] files = currentDir.listFiles();
                        for (int i = 0; i < files.length; i++) {
                            dataOut.println(files[i].getName());
                        }
                        dataOut.flush();
                        reply("226", "Transfer complete.");
                        dataOut.close();
                        dataSocket.close();
                        break;
                    //用戶端告知伺服器應該準備一個被動式的通道 由伺服器方建立一個傾聽通道
                    //伺服器建立完成後應回報所建立的IP位址與埠號給予用戶端
                    case PASV:
                        ServerSocket dataServer = new ServerSocket(0);
                        int dataPort = dataServer.getLocalPort();
                        System.out.println(dataPort);
                        int p1 = dataPort / 256;
                        int p2 = dataPort % 256;
                        /*reply("227", "Entering Passive Mode("
                                + serverIP.replace('.', ',') + "," + p1 + "," + p2 + ")." );*/
                        dataSocket = dataServer.accept();
                        System.out.println("已建立資料通道");
                        break;
                    //檔案下載的命令 後面接著是客戶端欲下載的檔案名稱
                    //客戶端傳送此命令之前必須先使用指令PASV 要求伺服器與客戶端先建立資料通道
                    //建立通道之後 RETR命令與LIST命令的方法類似 LIST命令的資料來源是目錄內的檔案名稱
                    //RETR的資料來源是特定檔案 因此當伺服器接收到RETR命令時 需先開啟該特定檔案
                    //一一讀取檔案內容並傳送至客戶端 傳送結束後送出回應碼226
                    case RETR:
                        String filename = stk.nextToken();
                        File file = new File("C:/net" + filename);
                        FileInputStream fileIn = new FileInputStream(file);
                        //121-129行 檢查欲下載的物件是否被鎖定中
                        try {
                            //嘗試取得FileLock物件
                            FileLock lock = fileIn.getChannel().tryLock();
                            //釋放剛取得的FileLock物件
                            lock.release();
                            //128-132行 若無法取得FileLock物件 代表該檔案仍在上傳中 暫時無法下載
                            //回應用戶端450碼 代表檔案目前無法存取 可稍後再嘗試下載該檔案
                        } catch (OverlappingFileLockException e) {
                            System.out.println("檔案正在上傳中,無法存取");
                            reply("450", "File busy.");
                            break;
                        }
                        reply("150", "Open connection for " + filename);
                        PrintWriter out = new PrintWriter(
                                dataSocket.getOutputStream());
                        int n = fileIn.read();
                        while (n != -1) {
                            out.write(n);
                            n = fileIn.read();
                        }
                        out.flush();
                        reply("226", "Transfer complete.");
                        out.close();
                        fileIn.close();
                        dataSocket.close();
                        break;
                    //在被動傳輸模式之下 上傳檔案至伺服器亦同樣需要建立資料通道 因此
                    //客戶端傳送PASV至伺服器端 請伺服器準備並回應資料通道的資訊後
                    //客戶端可使用STOR指令進行資料傳輸
                    //與下載不同 客戶端送出STRO指令後 伺服器在成功建立資料通道後即告知
                    //客戶端可以開始上傳 並立即準備接收客戶端持續傳來的資料並寫入檔案中
                    case STOR:
                        String fname = stk.nextToken();
                        reply("150", "Ok to send data.");
                        FileOutputStream dout = new FileOutputStream("C:/net/" + fname);
                        //為目前正上傳中的檔案建立FileLock鎖定物件
                        FileLock lock = dout.getChannel().tryLock();
                        InputStreamReader din = new InputStreamReader(
                                dataSocket.getInputStream());
                        int dn = din.read();
                        System.out.println("Data:" + dn);
                        while (dn != -1) {
                            dout.write(dn);
                            dn = din.read();
                        }
                        dout.flush();
                        reply("226", "File receive OK.");
                        dout.close();
                        //上傳完成 將鎖定物件解鎖
                        lock.release();
                        din.close();
                        dataSocket.close();
                        break;
                    default:
                        reply("500", "command not understood.");
                }
            }
        } catch (IOException e) {
            System.out.println("輸出入錯誤");
        }
    }

    //reply方法 用途是回應用戶端的命令
    public void reply(String code, String msg) {
        out.println(code + " " + msg);
        out.flush();
        System.out.println(" 回應:" + code);
    }

}