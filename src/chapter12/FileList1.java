package chapter12;

import java.io.File;

//12-6
//檔案物件在使用目錄時可呼叫list()方法 得到特定目錄下的檔案清單
public class FileList1 {
    public static void main(String[] args) {
        File f = new File("c:\\net");
        if (f.isDirectory()) {
            String[] files = f.list();
            for (int i = 0; i < files.length; i++) {
                System.out.println(files[i]);
            }
        }
    }
}
