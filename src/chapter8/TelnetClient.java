package chapter8;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
//8-25
public class TelnetClient {
    String host;
    int port;
    InputStream in;
    OutputStream out;
    //15-22行 為提高程式可讀性 預先定義代表各個TELNET協定中傳輸命令的常數
    public static final int IAC = 255;
    public static final int WILL = 251;
    public static final int WONT = 252;
    public static final int DO = 253;
    public static final int DONT = 254;
    public static final int TERMINAL_TYPE = 24;
    public static final int SB = 250;
    public static final int SE = 240;
    //24-27行 TelentClient類別的建構子 接收主機(host)與埠號(port)
    public TelnetClient(String host, int port) {
        this.host = host;
        this.port = port;
    }
    //29-35行 connect方法 產生連線至主機的socket物件 並取得輸入資料流物件in與輸出資料流out
    //最後進入處理的方法processLoop() 進行資料讀取與處理迴圈
    public void connect() throws UnknownHostException,
            IOException {
        Socket socket = new Socket(host, port);
        in = socket.getInputStream();
        out = socket.getOutputStream();
        processLoop();
    }
    //38-54行 資料讀取迴圈 持續讀取伺服器傳來的資料 並進行資料與命令的辨認
    public void processLoop() throws IOException {
        while (true) {
            int data = in.read();
            //42-50行 若傳來的資料為命令(IAC) 進行後續送達命令的解讀 若為SB則進行negotiation方法處理
            //若是DO Terminal時 則認同並進入Terminal-type選項的回應
            if (data == IAC) {
                int tone = in.read();
                int option = in.read();
                if (tone == SB)
                    negotiation(tone, option);
                if (tone == DO && option == TERMINAL_TYPE)
                    allowOption(tone, option);
                else
                    denyOption(tone, option);
                //53-55行 若為一般資料 則呼叫getBig5Char方法 處理中文Big5碼 並印出資料
            } else {
                System.out.println(getBig5Char(data, in));
            }
        }
    }
    //59-78行 此方法目的是拒絕伺服器傳來的選項要求
    //使用此方法可以因應傳來為WILL或DO而回復對應的拒絕(不認可)命令格式
    public void denyOption(int tone, int option)
            throws IOException {
        if (tone == DO) {
            System.out.println(IAC + "," + tone + "," + option);
            out.write(IAC);
            out.write(WONT);
            out.write(option);
            out.flush();
            System.out.println(" SENT:" + IAC + "," + WONT + ","
                    + option);
        } else if (tone == WILL) {
            System.out.println(IAC + "," + tone + "," + option);
            out.write(IAC);
            out.write(DONT);
            out.write(option);
            out.flush();
            System.out.println(" SENT:" + IAC + "," + DONT + ","
                    + option);
        }
    }
    //75-94行 allowOption方法目的是認可伺服器傳來的選項要求
    //使用此方法可以因應傳來為WILL或DO而回復對應的認可命令格式
    public void allowOption(int tone, int option)
            throws IOException {
        if (tone == DO) {
            System.out.println(IAC + "," + tone + "," + option);
            out.write(IAC);
            out.write(WILL);
            out.write(option);
            out.flush();
            System.out.println(" SENT:" + IAC + "," + WILL + ","
                    + option);
        } else if (tone == WILL) {
            System.out.println(IAC + "," + tone + "," + option);
            out.write(IAC);
            out.write(DO);
            out.write(option);
            out.flush();
            System.out.println(" SENT:" + IAC + "," + DO + ","
                    + option);
        }
    }
    //96-113行 實作傳送終端機模式的方法negotiation
    //以SB方式傳送"ANSI"為用戶端使用的終端機模式
    public void negotiation(int tone, int option)
            throws IOException {
        switch (option) {
            case TERMINAL_TYPE:
                in.read();
                in.read();
                in.read();
                //以SB方式送出"A N S I"
                out.write(IAC);
                out.write(SB);
                out.write(TERMINAL_TYPE);
                out.write(0);
                out.write("ANSI".getBytes());
                out.write(IAC);
                out.write(SE);
                out.flush();
        }
    }
    //123-133行 處理BIG5中文資料 判別第一個byte若大於127 則繼續讀入下一個byte 並組合為一個中文BIG5碼 再印出資料
    public char getBig5Char(int data, InputStream in)
            throws IOException {
        char c = (char) data;
        if (data > 127) {
            byte[] big5 = new byte[2];
            big5[0] = (byte) data;
            big5[1] = (byte) in.read();
            c = new String(big5, "BIG5").charAt(0);
        }
        return c;
    }
    //135-139行 測試本類別 連線至bbs.tku.edu.tw的23埠
    public static void main (String[] args)
            throws UnknownHostException, IOException {
        TelnetClient c = new TelnetClient("bbs.tku.edu.tw", 23);
        c.connect();
    }

}