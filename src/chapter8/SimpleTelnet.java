package chapter8;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

//8-15
public class SimpleTelnet {
    //12-16行 預先定義常數 以名稱代替不容易了解的整數值 提高日後程式碼的可讀性
    public static final int IAC = 255;
    public static final int WILL = 251;
    public static final int WONT = 252;
    public static final int DO = 253;
    public static final int DONT = 254;
    //本類別的執行方法main 此方法會拋出的例外物件
    public static void main (String[] args) throws UnknownHostException, IOException {
        //產生連往本機localhost的埠號23的連線物件s
        Socket s = new Socket("localhost", 23);
        //由s連線物件得到輸入資料流物件in
        InputStream in = s.getInputStream();
        //由s連線物件得到輸出資料流物件out
        OutputStream out = s.getOutputStream();
        //先讀取一個位元組 放在整數data中
        int data = in.read();
        //28-59行 讀取40個位元組的for迴圈
        for (int i = 0; i < 40; i++) {
            //30-51行 若是讀取到的資料是IAC 代表後續的資料是指令 應再將後續資料讀入 進行指令的判別
            if (data == IAC) {
                int tone = in.read();
                //33-41行 若指令的語氣為DO 則回應WONT 代表無法辨認該選項
                if (tone == DO) {
                    int option = in.read();
                    System.out.println(IAC + "," + tone + "," + option);
                    out.write(IAC);
                    out.write(WONT);
                    out.write(option);
                    out.flush();
                    System.out.println(" SENT:" + IAC + "," + WONT + "," + option);
                }
                //42-51行 若指令的語氣為WILL 則回應DONT 代表無法進行該選項的後續要求
                if (tone == WILL) {
                    int option = in.read();
                    System.out.println(IAC + "," + tone + "," + option);
                    out.write(IAC);
                    out.write(DONT);
                    out.write(option);
                    out.flush();
                    System.out.println(" SENT:" + IAC + "," + DONT + "," + option);
                }
                //53-55行 若讀取的資料非IAC時 則是一般文字 可轉型為字元char後印出
            } else {
                System.out.println(getBig5Char(data, in));
            }
            //讀取下一個資料後返回for迴圈
            data = in.read();
        }
    }
    //一個Big5中文碼是由兩個byte所組成的 而且Big5碼的第一個byte必定大於127
    //讀取每個位元組的過程中 若遇到大於127的位元組 則需要再讀入下一個位元組
    //並將兩個位元組合成一個字串 得到一個中文字
    //getBig5Char可以自動讀取正確Big5中文 它是個靜態方法(static)
    //並傳入一個位元組(目前讀到的資料)與輸入資料流
    public static char getBig5Char(int data, InputStream in) throws IOException {
        char c = (char) data;
        if (data > 127) {
            byte[] big5 = new byte[2];
            big5[0] = (byte) data;
            big5[1] = (byte) in.read();
            c = new String(big5, "BIG5").charAt(0);
        }
        return c;
    }

}