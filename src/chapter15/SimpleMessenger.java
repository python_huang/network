package chapter15;

import net.sf.jml.MsnMessenger;
import net.sf.jml.event.MsnMessengerListener;
import net.sf.jml.impl.MsnMessengerFactory;
//15-12
public class SimpleMessenger {
    public static void main (String[] args) {
        //欲使用JML登入MSN主機先要有一個主要的MsnMessenger物件 它是所有MSN通訊的基礎
        //產生該物件並不需要使用其建構子 而是使用MsnMessengerFactory類別的靜態方法createMsnMessenger
        //只要傳入登入帳號與密碼即可產生MsnMessenger物件
        String acc = "your@account.com";
        String pw = "mypassword";
        MsnMessenger msn =
                MsnMessengerFactory.createMsnMessenger(acc, pw);
        //17-31行 為msn物件註冊一個傾聽者物件 利用匿名類別方式 實作傾聽者介面與其方法
        msn.addMessengerListener(new MsnMessengerListener() {
            //19-22行 覆寫MsnMessengerListener介面的logout方法
            @Override
            public void logout(MsnMessenger msn) {

            }
            //24-27行 當msn物件登入成功時將執行此方法內容
            @Override
            public void loginCompleted(MsnMessenger msn) {
                System.out.println("登入成功");
            }
            //29-32行 當msn物件登入失敗時將執行此方法內容 並傳入發生的例外物件msn
            @Override
            public void exceptionCaught(MsnMessenger msn, Throwable e) {
                System.out.println("登入失敗");
            }
        });
        //MsnMessenger的login()方法可利用先前傳入的帳號與密碼進行登入 省去了自行實作繁複的通訊協定程式碼
        //呼叫此方法後雖然後面沒有其他的程式碼 main()方法應該是結束了 可是程式所產生的執行緒仍在執行當中
        msn.login();
    }
}
