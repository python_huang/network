package chapter15;

import net.sf.jml.MsnContact;
import net.sf.jml.MsnContactList;
import net.sf.jml.MsnMessenger;
import net.sf.jml.event.MsnAdapter;
import net.sf.jml.impl.MsnMessengerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.EventQueue;
//15-32
public class MsnFrame {
    private JFrame frame;
    String acc;
    String pw;
    MsnMessenger msn;
    JLabel status = new JLabel("離線");
    private JList list;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MsnFrame window = new MsnFrame();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MsnFrame() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 311, 209);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel northPanel = new JPanel();
        frame.getContentPane().add(northPanel, BorderLayout.NORTH);
        JLabel label = new JLabel("狀態:");
        northPanel.add(label);
        northPanel.add(status);
        JButton loginButton = new JButton("登入");
        loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                LoginDialog dialog = new LoginDialog();
                dialog.setModal(true);
                dialog.setVisible(true);
                acc = dialog.id.getText();
                pw = dialog.pw.getText();
                msn = MsnMessengerFactory.createMsnMessenger(acc, pw);
                msn.addListener(new MsnAdapter() {
                    public void loginCompleted(MsnMessenger msn) {
                        System.out.println("登入成功");
                    }
                    public void contactListInitCompleted(MsnMessenger msn)
                    {
                        MsnContactList contactList = msn.getContactList();
                        MsnContact[] contacts = contactList.getContacts();
                        DefaultListModel listData = new DefaultListModel();
                        for (MsnContact c : contacts) {
                            listData.addElement(c.getDisplayName());
                        }
                        System.out.println(listData);
                        list.setModel(listData);
                    }

                    public void ownerStatusChanged(MsnMessenger msn) {
                        String info = msn.getOwner().getStatus()
                                .toString();
                        status.setText(info);
                    }
                });
                msn.login();
            }
        });
        northPanel.add(loginButton);
        JScrollPane scrollPane = new JScrollPane();
        frame.getContentPane()
                .add(scrollPane, BorderLayout.CENTER);
        list = new JList();
        scrollPane.setViewportView(list);
    }

}
