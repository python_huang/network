package chapter15;

import  net.sf.jml.MsnContact;
import  net.sf.jml.MsnMessenger;
import  net.sf.jml.MsnSwitchboard;
import  net.sf.jml.event.MsnMessageAdapter;
import  net.sf.jml.impl.MsnMessengerFactory;
import  net.sf.jml.message.MsnInstantMessage;
//15-23
public class EchoRobot {
    public static void main(String[] args) {
        //13-14行 EchoRobot機器人使用的MSN帳號與密碼
        String account = "yout@account.com";
        String password = "mypassword";
        //16-17行 產生msn物件
        MsnMessenger msn = MsnMessengerFactory.createMsnMessenger(
                account, password);
        //19-33行 為msn物件註冊一個訊息傾聽者 利用MsnMessageAdapter適配器實作匿名類別
        msn.addMessageListener(new MsnMessageAdapter() {
            //22-27行 當收到一般訊息時會自動執行此方法 在方法中先取得收到的訊息字串msg.getContext()後印出提示
            //利用MsnSwitchBoard sb物件將回復訊息傳送給聯絡人
            public void instantMessageReceived(MsnSwitchboard sb,
                                               MsnInstantMessage msg, MsnContact contact) {
                String text = msg.getContent();
                System.out.println("收到訊息:" + text);
                sb.sendText("*" + text + "*");
            }
            //29-32行 當收到離線訊息時會自動執行此方法
            public void offlineMessageReceived(String body,
                                               String ctype, String encoding, MsnContact contact) {
                System.out.println("收到離線訊息:" + body);
            }
        });
        //登入MSN網路
        msn.login();
    }
}
