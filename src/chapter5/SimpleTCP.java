package chapter5;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
//當Socket物件成功建立後 客戶端(程式)與伺服器端(遠端主機)可開始進行資料的傳輸
//此時要利用java.io套件庫內的串流類別(Stream) 如簡單的InputStream/OutputStream
//或支援亞洲字元的Reader/Writer類別 Socket類別可比喻為插頭 而串流類別是由插頭
//衍生出來的電線
//由Socket物件取得串流物件的方法為getInputStream與getOutputStream
//5-12
public class SimpleTCP {
    public static void main(String[] args) {
        try {
            Socket ptt = new Socket("ptt.cc", 23);
            InputStream in = ptt.getInputStream();
            OutputStream out = ptt.getOutputStream();
            in.close();
            out.close();
            ptt.close();
        } catch (UnknownHostException e) {
            System.out.println("主機連線失敗");
        } catch (IOException e) {
            System.out.println("傳輸失敗");
        }
    }
}
