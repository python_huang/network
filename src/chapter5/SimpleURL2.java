package chapter5;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

//5-21
public class SimpleURL2 {
    public static void main(String[] args) throws IOException {
        //連線到下列網址並取得資料
        //產生URL物件
        URL url = new URL("http", "j.snpy.org", "/net/index.html");
        //取得URLConnection連線物件
        URLConnection conn = url.openConnection();
        //與該資源建立實質連線
        conn.connect();
        //取得內文格式
        String type = conn.getContentType();
        System.out.println("內文格式:" + type);
        //利用字串類別的方法lastIndexOf("=")找到最後一個等號
        //再以字串方法substring()取得編碼字串("UTF-8")
        String encoding = type.substring(type.lastIndexOf("=") + 1);
        System.out.println("內文編碼:" + encoding);
        //將輸入資料流轉為Reader, 並指定來源編碼格式
        InputStreamReader in = new InputStreamReader(conn.getInputStream(), encoding);
        //以迴圈取得所有資料
        int data = in.read();
        while (data != -1) {
            System.out.print((char) data);
            data = in.read();
        }
        //關閉資料流物件
        in.close();
    }
}