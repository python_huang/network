package chapter5;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
//本機產生一個連線socket物件 連線至www.snpy.org的埠號80
//並取得本連線的本地與遠端的位址資訊
//5-10
public class SimpleSocket2 {
    public static void main (String[] args) throws UnknownHostException, IOException{
        Socket socket = new Socket("www.snpy.org", 80);
        System.out.println("本地位址:" + socket.getLocalAddress());
        System.out.println("本地埠號:" + socket.getLocalPort());
        System.out.println("遠端位址:" + socket.getInetAddress());
        System.out.println("遠端埠號:" + socket.getPort());
    }
}
