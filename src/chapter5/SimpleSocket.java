package chapter5;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
//由本地主機的埠號2222出發的連線 連至遠端主機ppt.cc的埠號23
//由於使用的timeout值很短(5毫秒) 本範例執行後會拋出java.net.SocketTimeoutException例外
//5-9
public class SimpleSocket {
    public static void main (String[] args) throws IOException {
        Socket socket = new Socket();
        InetSocketAddress local = new InetSocketAddress(2222);
        InetSocketAddress ad = new InetSocketAddress("ptt.cc", 23);
        socket.bind(local);
        System.out.println("本地埠號:" + socket.getLocalPort());
        socket.connect(ad, 5);
    }
}
