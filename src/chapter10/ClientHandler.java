package chapter10;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Map;
import java.util.TreeMap;
//10-20
//執行TelnetServerNio類別後即開始等待用戶端連線 此時使用Windows的命令提示字元
//輸入telent localhost 9980連線至伺服器測試
public class ClientHandler {
    private Map<String, String> users;
    SocketChannel channel;
    ByteBuffer buff = ByteBuffer.allocate(100);
    public static final int IAC = 255;
    public static final int WILL = 251;
    public static final int WONT = 252;
    public static final int DO = 253;
    public static final int DONT = 254;
    public static final int STAGE_USER = 0;
    public static final int STAGE_PASSWORD = 1;
    public static final int STAGE_LOGON = 2;
    int stage = STAGE_USER;
    String userid = "";
    String pw = "";

    public ClientHandler(SelectionKey key) {
        channel = (SocketChannel) key.channel();
        initAccount();
    }
    //32-92行 重點方法handleRead() 處理並解讀用戶端傳送來的資料 若為TELNET指令則呼叫handleCommand專門處理
    //若為一般資料 則利用階段(stage)方式分別取得帳號userid與密碼pw
    public void handleRead(SelectionKey key) {
        try {
            //由通道讀取資料至buff物件中
            int count = channel.read(buff);
            if (count > 0) {
                //若有讀取資料 則將buff物件的讀取指標移動至第0個位置 準備後續一一處理
                buff.flip();
                //42-86行 使用while迴圈處理由用戶端傳來的資料 並辨識其為指令或一般資料
                while (buff.position() < count) {
                    int data = buff.get() & 0xFF;
                    //45-46行 若傳來資料為TELNET指令 則由handleCommand方法處理並回應
                    if (data == IAC) {
                        handleCommand(buff);
                        //49-85行 若傳來為一般資料 則分為帳號輸入時期(STAGE_USER)
                        //與密碼輸入時期(STAGE_PASSWORD)兩個階段 分別取得帳號userid與密碼pw字串
                    } else {
                        //51-84行 以switch方式對不同時期以不同方式取得資料
                        switch (stage) {
                            //54-64行 輸入帳號時期 等待使用者按下Enter鍵 Enter鍵被解讀為整數13與10 當資料為整數值10
                            //代表使用者按下Enter鍵 去掉先前的整數13後 得到userid字串
                            case STAGE_USER:
                                if (data == 10) {
                                    send("\n\rpassword");
                                    stage = STAGE_PASSWORD;
                                    userid = userid.substring(0,
                                            userid.length() - 1);
                                    System.out.println("id="+userid);
                                } else {
                                    userid = userid + (char) data;
                                }
                                break;
                                //66-83行 輸入密碼時期 等待使用者按下Enter鍵並取得pw字串
                            case STAGE_PASSWORD:
                                if (data == 10) {
                                    pw = pw.substring(0, pw.length() - 1);
                                    System.out.println("pw=" + pw);
                                    //由於userid與pw字串皆已解析後得到資料 此時進行模擬帳號驗證
                                    //若符合Map集合中的帳號資料則送出"Login successful" 告知用戶端登入成功
                                    //若不符合 則送出"Login failed"並斷線
                                    if (users.get(userid).equals(pw)){
                                        send("\n\rLogin Successful");
                                        stage = STAGE_LOGON;
                                    } else {
                                        send("\n\rLogin failed");
                                        channel.close();
                                    }
                                } else {
                                    pw = pw + (char) data;
                                }
                                break;
                        }
                    }
                }
                buff.clear();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //當用戶端連線成功後開始傳輸資料時 送出歡迎訊息 並提示用戶端該進行帳號的輸入
    public void welcomeMessage() {
        send("Welcome\r\nlogin:");
        stage = STAGE_USER;
    }

    public void send (String text) {
        ByteBuffer buff = ByteBuffer.allocate(1024);
        buff.put(text.getBytes());
        buff.flip();
        try {
            channel.write(buff);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //當用戶端傳來資料為TELNET指令(IAC)時 解析指令並回應否定選項的方法
    public void handleCommand(ByteBuffer buf)
        throws IOException {
        ByteBuffer outBuf = ByteBuffer.allocate(1024);
        int tone = buf.get() & 0xFF;
        int option = buf.get() & 0xFF;
        outBuf.clear();
        if (tone == DO) {
            outBuf.put((byte) IAC);
            outBuf.put((byte) WONT);
            outBuf.put((byte) option);
            outBuf.flip();
            channel.write(outBuf);
        } else if (tone == WILL) {
            outBuf.put((byte) IAC);
            outBuf.put((byte) DONT);
            outBuf.put((byte) option);
            outBuf.flip();
            channel.write(outBuf);
        }
    }
    //準備一個Map集合 存放兩筆測試用的帳號 以帳號名稱為鍵(key) 密碼為值(value) 用來測試後續用戶端傳送帳號資料時
    //可進行登入驗證
    public void initAccount() {
        users = new TreeMap<String, String>();
        users.put("tom", "uu123");
        users.put("jack", "yy123");
    }

}