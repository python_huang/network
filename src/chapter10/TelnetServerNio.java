package chapter10;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;
//10-17
public class TelnetServerNio {
    int port = 9980;

    public TelnetServerNio(int port) {
        this.port = port;
    }
    //18-66行 呼叫此方法後 伺服器將持續傾聽9980埠 若用戶端連線成功 則為該用戶端產生一個專屬協定處理物件ClientHandler
    //針對用戶端傳輸資料進行解讀與服務
    public void accept() {
        try {
            ServerSocketChannel serverChannel =
                    ServerSocketChannel.open();
            ServerSocket ss = serverChannel.socket();
            ss.bind(new InetSocketAddress(port));
            serverChannel.configureBlocking(false);
            Selector selector = Selector.open();
            serverChannel.register(selector, SelectionKey.OP_ACCEPT);
            while (true) {
                selector.select();
                Set keys = selector.selectedKeys();
                Iterator it = keys.iterator();
                int count = 0;
                while (it.hasNext()) {
                    SelectionKey key = (SelectionKey) it.next();
                    it.remove();
                    //36-50行 若用戶端連線則為其準備協定處理物件
                    if (key.isAcceptable()) {
                        System.out.println("用戶連上線");
                        ServerSocketChannel server =
                                (ServerSocketChannel) key.channel();
                        SocketChannel client = server.accept();
                        client.configureBlocking(false);
                        SelectionKey clientKey = client.register(selector,
                                SelectionKey.OP_READ);
                        //45-46行 產生ClientHandler物件,專門處理用戶端送達的資料
                        ClientHandler handler =
                                new ClientHandler(clientKey);
                        //呼叫ClientHandler的welcomeMessage()方法 傳送歡迎訊息給用戶端 並提示用戶端進行帳號登入
                        handler.welcomeMessage();
                        //將用戶協定處理物件ClientHandler物件附加於clientKey鍵中
                        clientKey.attach(handler);
                        //52-58行 若用戶端傳來資料(已連線完成) 則由ClientHandler的handleRead方法處理資料
                    } else if (key.isReadable()) {
                        //54-55行 由key鍵值中取出附加物件,並轉型為ClientHandler
                        ClientHandler handler =
                                (ClientHandler) key.attachment();
                        //由ClientHandler物件解讀用戶端送達的資料
                        handler.handleRead(key);
                    }
                }
            }
        } catch (ClosedChannelException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new TelnetServerNio(9980).accept();
    }

}