package chapter10;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
//10-6
public class TelnetTester {
    public static final int IAC = 255;
    public static final int WILL = 251;
    public static final int WONT = 252;
    public static final int DO = 253;
    public static final int DONT = 254;
    public static final int TERMINAL_TYPE = 24;
    public static final int SB = 250;
    public static final int SE = 240;

    public static void main(String[] args) throws IOException {
        //用InetSocketAddress類別建立物件 傳入[主機位址或名稱]與[埠號]
        InetSocketAddress addr = new InetSocketAddress(
                "localhost", 23);
        //呼叫SocketChannel的open方法 產生與目的地主機的連線
        SocketChannel chann = SocketChannel.open(addr);
        //準備緩衝區物件(ByteBuffer) 並指定其容量為1024個位元組
        ByteBuffer buf = ByteBuffer.allocate(1024);
        //完成連線與緩衝區的準備工作後 可從連線端將資料讀取至緩衝區內 此時使用SocketChannel的read方法
        chann.read(buf);
        System.out.println("緩衝區有效資料個數:" + buf.position());
        //讀取資料後呼叫flip()方法 將緩衝區的讀取指標位置移至第一個位置 準備後續循序讀取處理
        buf.flip();
        //利用while迴圈循序讀取緩衝區內的位元組 並顯示於主控台上
        while (buf.hasRemaining()) {
            byte b = buf.get();
            int data = b & 0xFF;
            if (data == IAC) {
                System.out.println();
                System.out.print(data + ",");
                handleCommand(chann, buf);
            } else {
                System.out.print((char) data);
            }
        }
    }
    //45-46行 定義handleCommand的規格 接收兩個參數 一是SocketChannel物件chann 另一個是緩衝區物件buf
    public static void handleCommand (SocketChannel chann,
                                      ByteBuffer buf) throws IOException {
        //預先準備傳送資料專用緩衝區outBuf
        ByteBuffer outBuf = ByteBuffer.allocate(1024);
        //讀取IAC後面的語氣值 可能是DO或WILL
        int tone = buf.get() & 0xFF;
        //讀取語氣值後面的選項
        int option = buf.get() & 0xFF;
        System.out.println(tone + "," + option);
        outBuf.clear();
        //56-63行 當伺服器傳來請求(DO)執行時 回應否定(WONT)該選項
        if (tone == DO) {
            outBuf.put((byte) IAC);
            outBuf.put((byte) WONT);
            outBuf.put((byte) option);
            outBuf.flip();
            chann.write(outBuf);
            System.out.println(" SENT:" + IAC + "," + WONT + ","
                    + option);
            //65-73行 當伺服器傳來想要(WILL)執行時 回應拒絕(DONT)該選項
        } else if (tone == WILL) {
            outBuf.put((byte) IAC);
            outBuf.put((byte) DONT);
            outBuf.put((byte) option);
            outBuf.flip();
            chann.write(outBuf);
            System.out.println(" SENT:" + IAC + "," + DONT + ","
                    + option);
        }
    }

}
//IAC DO AUTH 請執行認證選項
//IAC WILL ECHO 想要執行回音選項
//IAC WILL SG 想要執行subnegotiation選項
//IAC DO NEW-ENV 請執行傳送環境變數選項
//IAC DO NAWS 請執行客戶端畫面資訊選項
//IAC DO BINARY 請執行二元制資料傳送選項
//IAC WILL BINARY 想要執行二元制資料傳送選項