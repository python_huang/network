package chapter13;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.*;
import java.io.IOException;
//13-20
public class HttpClientGet {
    //13-14行 定義本測試類別的main方法 因為HttpClient的execute()方法會拋出ClientProtocolException與IOException例外
    //為簡化程式碼 故在此定義main方法將向上層拋出例外
    public static void main(String[] args)
        throws ClientProtocolException, IOException {
        //使用DefaultHttpClient類別的建構子產生HttpClient物件
        HttpClient httpclient = new DefaultHttpClient();
        //18-20行 以一個URL網址字串再加上參數 產生一個HttpGet請求物件httpget
        String params = "id=119&func=ac929";
        HttpGet httpget = new HttpGet(
                "http://j.snpy.org/net/get.php?" + params);
        //利用HttpClient定義的execute方法連線至網頁伺服器 並在連線完成後得到HttpResponse物件
        HttpResponse response = httpclient.execute(httpget);
        //印出HTTP回應的狀態字串
        System.out.println(response.getStatusLine());
    }
}
