package chapter13;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
//13-13
public class SimplePost {
    public static void main(String[] args) throws IOException {
        String params = "id=119&func=ac929";
        //14-15行 建立連線
        URL url = new URL("http://j.snpy.org/net/post.php");
        URLConnection conn = url.openConnection();
        //設定連線為允許輸出
        conn.setDoOutput(true);
        //19-20行 由conn連線物件取得輸出資料流 再轉為Writer類別
        OutputStreamWriter out = new OutputStreamWriter(
                conn.getOutputStream());
        //先送出參數資料
        out.write(params);
        System.out.println("送出參數: " + params);
        out.flush();
        Reader in = new InputStreamReader(conn.getInputStream(),
                "UTF-8");
        int data = in.read();
        //29-32行 依序讀取傳入的HTML檔案內容
        while (data != -1) {
            System.out.print((char) data);
            data = in.read();
        }
    }
}
