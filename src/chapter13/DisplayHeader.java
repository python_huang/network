package chapter13;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

//13-8
public class DisplayHeader {
    public static void main (String[] args) {
        try {
            URL url = new URL("http://j.snpy.org/net/index.html");
            URLConnection conn = url.openConnection();
            System.out.println("Object's Class name =>"
                    + url.getClass().getName());
            System.out.println("Content Type:"
                    + conn.getContentType());
            System.out.println("Content Length:"
                    + conn.getContentLength());
            System.out.println("Date:" + new Date(conn.getDate()));
            System.out.println("Expires:" + conn.getExpiration());
            System.out.println("第3項資料:" + conn.getHeaderField(3));
            System.out.println("最後修改時間:"
                    + conn.getHeaderField("Last-Modified"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
//String getContentType() 取得網頁內容(本文)的格式 除了告知客戶端該資源是屬於那一類資料外 亦告知其邊碼格式
//例如"text/html;charset=UTF-8"代表該資源是網頁html文件 且其內容是以UTF-8編碼

//int getContentLength() 取得網頁內容的資料長度 回傳整數值

//long getDate() 取得資源傳送日期與時間 得到的是長整數值 是由西元1970年1月1日至現在的毫秒數
//應轉換為java.util.Date類別較容易辨識

//long getExpiration() 取得該資源有效日期與時間的長整數值 若無該欄位資料時 則回傳0
//String getHeaderField(int n) 依傳入的索引值取得對應的欄位資料
//String getHeaderField(String name) 依傳入的欄位名稱取得對應的欄位資料