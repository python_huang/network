package chapter13;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

//13-6
//在與遠端伺服器上的資源建立連線後 可利用getInputStream()方法取得輸入資料流InputStream物件
//可以用它來讀入網頁資源的內容 InputStream類別是java IO套件的主要類別 用來依序以byte為
//單位讀取資料 使用資料流的方法read()可以讀取一個整數值 當所有資料都讀取完畢
//再讀取時將得到整數-1
//若要讀取中文資料 將21行由InputStream類別改為Reader類別 並指定編碼UTF-8
public class SimpleHttp {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://j.snpy.org/net/index.html");
            URLConnection conn = url.openConnection();
            Reader in = new InputStreamReader(conn.getInputStream(), "UTF-8");
            int data = in.read();
            while (data != -1) {
                System.out.print((char) data);
                data = in.read();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
