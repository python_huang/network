package chapter13;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

//13-10
public class SimpleGet {
    public static void main(String[] args) throws IOException {
//將中文字串轉換為UTF-8格式 再以HTTP GET方法送至伺服器 再由伺服端傳回參數資料的網頁
//        String name = "王小明";
//        String utf8 = URLEncoder.encode(name, "UTF-8");
//        String params = "id=" + utf8;
//        System.out.println("網址參數(UTF-8): " + params);
        String params = "id=119&func=ac929";
        URL url = new URL("http://j.snpy.org/net/get.php?"
                + params);
        URLConnection conn = url.openConnection();
        Reader in = new InputStreamReader(conn.getInputStream(),
                "UTF-8");
        int data = in.read();
        while (data != -1) {
            System.out.print((char) data);
            data = in.read();
        }
    }
}
