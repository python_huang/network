package chapter14;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
//14-14
public class Mailer {
    //為Mailer類別定義一個session屬性
    Session session;

    //14-22行 Mailer類別的建構子
    public Mailer(String prot, String server) {
        //取得系統變數集合物件props
        Properties props = System.getProperties();
        //18-19行 設定寄件時所需要的系統變數
        props.put("mail.smtp.host", "msa.hinet.net");
        props.put("mail.transport.protocol", "smtp");
        //取得session物件
        session = Session.getDefaultInstance(props);
    }

    public void send(String from, String to, String subject,
                     String text) {
        //利用Mailer的屬性session 產生msg訊息物件
        Message msg = new MimeMessage(session);
        try {
            //30-33行 將傳入參數字串from與to物件 轉換為位址物件fromAddress與toAddress 並設定至msg訊息中
            InternetAddress fromAddress = new InternetAddress(from);
            InternetAddress toAddress = new InternetAddress(to);
            msg.setFrom(fromAddress);
            msg.setRecipient(Message.RecipientType.TO, toAddress);
            //35-36行 設定標題與信件內容
            msg.setSubject(subject);
            msg.setText(text);
            //傳送msg物件
            Transport.send(msg);
        } catch (AddressException e) {
            e.printStackTrace();
            System.out.println("郵件位址錯誤");
        } catch (MessagingException e) {
            e.printStackTrace();
            System.out.println("訊息錯誤");
        }
    }

    public Message[] read(String host, String user, String pw) {
        Message[] messages = null;
        try {
            Store store = session.getStore("pop3");
            store.connect(host, user, pw);
            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_ONLY);
            messages = inbox.getMessages();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
            System.out.println("讀信協定錯誤");
        } catch (MessagingException e) {
            e.printStackTrace();
            System.out.println("訊息錯誤");
        }
        return messages;
    }
    //先產生Mailer物件 再使用send方法即可寄送郵件
    public static void main(String[] args) {
        Mailer mailer = new Mailer("smtp", "msa.hinet.net");
        mailer.send("jack@test.com", "", "第一封測試信",
                "測試訊息內容(1)");
        mailer.send("tom@abc.com", "", "第二封測試信",
                "測試訊息內容(2)");
    }

}