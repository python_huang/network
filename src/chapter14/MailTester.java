package chapter14;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

//14-12
public class MailTester {
    public static void main(String[] args) throws MessagingException{
        //使用JavaMail送信時 要求開發人員準備一個系統變數的集合(Properties物件)
        //該集合中必需有[mail.host]與[mail.transport.protocol]兩格系統變數值
        java.util.Properties props = System.getProperties();
        //送信時要與哪一台伺服器主機連線 此值應該是目前執行環境的SMTP伺服器
        //若使用中華電信的寬頻上網 "msa.hinet.net"為該網路服務商所提供的送信主機
        //若在學校執行此程式 則應改為該學校的SMTP主機的名稱
        props.put("mail.smtp.host", "msa.hinet.net");
        //送信主機使用的送信協定 可使用的有"smtp" "pop"或"imap"
        props.put("mail.transport.protocol", "smtp");
        //當程式連線至送信伺服器後 將進行一連串的協定交談 這段時間是協定的互動期
        //javax.mail.Session類別代表與送信伺服器之間的交談期間 session擁有與伺服器交談所需要的[系統變數集合(Properties)]
        //session物件可由Session.getInstance(props, null)或是getDefaultInstance()方法取得
        javax.mail.Session session =
                javax.mail.Session.getDefaultInstance(props);
        //電子郵件的資訊稱為[訊息(Message)] 包括送件者 收件者 訊息標題 與訊息的內容等資訊
        //送件者與收件者並不使用字串型態 而是多了一個javax.mail.internet.InternetAddress類別來表示這類資訊
        InternetAddress from = new InternetAddress("jack@test.com");
        //請更改為收的到信的郵件地址
        InternetAddress to = new InternetAddress("");
        //JavaMail為訊息設計了javax.mail.Message抽象類別
        //亦在javax.mail.internet套件下設計了子類別MimeMessage供開發人員使用 利用MimeMessage類別的建構子產生物件
        Message msg = new MimeMessage(session);
        //設計訊息內的送件者資訊
        msg.setFrom(from);
        //設計訊息內的收件者資訊
        msg.setRecipient(Message.RecipientType.TO, to);
        //設計訊息內的送件者資訊
        msg.setSubject("mail for testing");
        //設計訊息內的送件者資訊
        msg.setText("測試訊息內容");
        //利用JavaMail提供的javax.mail.Transport類別進行信件的傳送 它實作了送出郵件的靜態方法send()
        Transport.send(msg);
        System.out.println("成功寄出");
    }
}
