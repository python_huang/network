package chapter14;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Vector;




//14-21
public class MailFrame {
    //定義屬性frame 是一個應用程式視窗JFrame類別
    private JFrame frame;
    //定義屬性table 為swing的表格類別JTable
    private JTable table;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    //28-29行 產生MailFrame物件 並顯示frame應用程式視窗
                    MailFrame window = new MailFrame();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MailFrame() {
        initialize();
    }
    //41-89行 配置整個視窗 包括按鈕button並撰寫事件處理程式
    private void initialize() {
        frame = new JFrame();
        //44-45行 配置視窗大小 並設定關閉時的動作
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel leftPanel = new JPanel();
        frame.getContentPane().add(leftPanel, BorderLayout.WEST);
        JButton button = new JButton("讀取信件");
        button.addActionListener(new ActionListener() {
            //51-80行 註冊事件傾聽者 使用匿名類別設計模式 當按下按鈕時 產生Mailer物件讀取所有信件
            public void actionPerformed(ActionEvent e) {
                Vector rows = new Vector();
                Mailer mailer = new Mailer("smtp", "msa.hinet.net");
                Message[] messages = mailer.read("snpy.org", "eric",
                        "mypassword");
                //57-69行 利用for迴圈將每封信件的資訊轉換為Vector物件data 再收集到Vector物件rows中
                for (Message msg : messages) {
                    Vector data = new Vector();
                    try {
                        data.add(msg.getFrom()[0]);
                        data.add(msg.getSubject());
                        data.add(msg.getContent());
                        rows.add(data);
                    } catch (MessagingException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                //71-74行 準備表格標題Vector物件ColumnNames
                Vector columnNames = new Vector();
                columnNames.add("寄件人");
                columnNames.add("標題");
                columnNames.add("內容");
                //77-78行 利用資料集合rows與表格標題欄位名稱集合兩項資料 產生DefaultTableModel物件
                //並指定至表格物件Table中
                table.setModel(
                        new DefaultTableModel(rows, columnNames));
            }
        });
        //82-86行 配置左方面板leftPanel與中間面板centerPanel
        leftPanel.add(button);
        JPanel centerPanel = new JPanel();
        frame.getContentPane().add(centerPanel,
                BorderLayout.CENTER);
        centerPanel.setLayout(new BorderLayout(0, 0));
        table = new JTable();
        centerPanel.add(table);
    }

}
