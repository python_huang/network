package chapter14;

import javax.mail.MessagingException;

//14-8
//測試是否能正常使用JavaMail的類別庫
public class MailTester0 {
    public static void main(String[] args)
        throws MessagingException {
        java.util.Properties props = System.getProperties();
        javax.mail.Session session = javax.mail.Session
                .getDefaultInstance(props);
    }
}
