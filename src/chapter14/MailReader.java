package chapter14;

import javax.mail.*;
import java.io.IOException;
import java.util.Properties;

//14-18
//讀取INBOX信件的類別 執行後以POP3協定連結至eric@snpy.org信箱 並將INBOX目錄內的所有信件以原始格式印出
public class MailReader {
    public static void main(String[] args) throws
            MessagingException {
        Properties props = System.getProperties();
        Session session = Session.getDefaultInstance(props);
        Store store = session.getStore("pop3");
        store.connect("snpy.org", "eric", "mypassword");
        Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_ONLY);
        Message[] messages = inbox.getMessages();
        for (Message msg : messages) {
            try {
                msg.writeTo(System.out);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("輸出入錯誤");
            }
        }
    }
}
