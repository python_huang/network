package chapter4;

import java.io.FileWriter;
import java.io.IOException;

//產生名data44的txt檔 內容:這是中文資料
//4-27
public class SimpleFile44 {
    public static void main(String[] args) throws IOException {
        FileWriter out = new FileWriter("data44.txt");
        out.write("這是中文資料");
        out.flush();
        out.close();
    }
}