package chapter4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

//產生名data的txt檔 內容:AXYZ  [A]的ASCII碼是65
//4-23
public class SimpleFileOutput {
    public static void main(String[] args) throws IOException {
        PrintStream out = new PrintStream("data.txt");
        out.write(65);
        out.print("XYZ");
        out.flush();
        out.close();
    }
}