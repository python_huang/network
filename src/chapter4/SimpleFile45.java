package chapter4;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

//4-27
public class SimpleFile45 {
    public static void main(String[] args) throws FileNotFoundException {
        //產生文字檔data45.txt
        PrintWriter out = new PrintWriter("data45.txt");
        //內容為:
        out.println("測試資料第一行");
        out.println("測試資料第二行");
        out.println("測試資料第三行");
        out.println("測試資料第四行");
        out.println("測試資料第五行");
        out.flush();
        out.close();
    }
}