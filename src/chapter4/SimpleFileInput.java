package chapter4;

import java.io.FileInputStream;
import java.io.IOException;

//讀取data.txt檔案 讀取內容的byte 顯示為65
//4-25
public class SimpleFileInput {
    public static void main(String[] args) throws IOException {
        FileInputStream in = new FileInputStream("data.txt");
        int n = in.read();
        System.out.println(n);
    }

}