package chapter4;

import java.io.FileNotFoundException;
import java.io.FileReader;
//例外出現時可以選擇不處理 而是讓例外往上層呼叫來源拋出
//如果最後都沒有處理例外 會在執行過程中被Java虛擬機器抓到 而中斷程式執行
//4-41
public class ExceptionHandling411 {
    //為main定義throws例外
    public static void main (String[] args) throws FileNotFoundException {
        FileReader br = new FileReader("data45.txt");
    }
}