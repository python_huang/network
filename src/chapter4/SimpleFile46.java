package chapter4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

//4-30
public class SimpleFile46 {
    public static void main(String[] args) throws IOException {
        FileReader br = new FileReader("data45.txt");
        BufferedReader in = new BufferedReader(br);
        String line = in.readLine();
        while (line != null) {
            System.out.println(line);
            line = in.readLine();
        }
    }
}
//BufferedReader有緩衝讀取資料功能
//readline可一次讀取一行字串 讀到尾端會得到null 可用while迴圈 判斷字串line是否為null