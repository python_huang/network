package chapter4;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

//InputStreamReader與OutputStreamWriter屬於[由byte轉成char]的中繼接頭
//byte轉換為char 常見的轉換方法 讀取data45.txt所有資料的範例
//4-31
public class SimpleFile47 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("data45.txt");
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader in = new BufferedReader(isr);
        String line = in.readLine();
        while (line != null) {
            System.out.println(line);
            line = in.readLine();
        }
    }
}