package chapter4;

import java.io.FileNotFoundException;
import  java.io.FileReader;
//使用FileReader讀取檔案 有可能因為不存在或檔案系統出問題而出現例外
//所以使用try...catch區塊預先處理例外的發生
//4-39
public class ExceptionHanding410 {
    public static void main (String[] args) {
        try {
            FileReader br = new FileReader("data47.txt");
        } catch (FileNotFoundException e) {
            System.out.println("檔案不存在,請檢查檔案名稱.");
        }
    }
}
