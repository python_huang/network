package chapter9;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
//9-13
public class NioFtpClient {
    public static void main(String[] args) throws IOException {
        InetSocketAddress addr =
                new InetSocketAddress("j.snpy.org", 21);
        //12-14行 先產生InetSocketAddress位址物件 再以位址物件產生連線至該主機的SocketChannel
        SocketChannel chann = SocketChannel.open(addr);
        //準備緩衝區物件buf 共1024個位置大小的空間
        ByteBuffer buf = ByteBuffer.allocate(1024);
        //讀取由遠端主機傳來的資料 並放在buf中
        chann.read(buf);
        System.out.println("緩衝區有效資料個數:"+buf.position());
        //命令傳送 帳號認證
        //將緩衝區的位置值歸零 以便後續可再放新的資料
        buf.clear();
        //將認證字串放入緩衝區
        buf.put("USER anonymous\n".getBytes());
        //將緩衝區的位置值設為資料前端 以便後續將緩衝區資料傳送出去
        buf.flip();
        //傳送緩衝區資料至FTP伺服器端
        chann.write(buf);
        //清除緩衝區
        buf.clear();
        //再次讀取資料
        chann.read(buf);
        //呼叫flip()方法 將緩衝區的位置設定為0 限制值設為有效資料的後一位置值
        buf.flip();
        //利用迴圈將所有有效資料一一印出
        while (buf.hasRemaining()) {
            System.out.print((char)buf.get());
        }
    }
}
