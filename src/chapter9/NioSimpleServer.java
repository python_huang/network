package chapter9;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
//9-20
public class NioSimpleServer {
    public static void main (String[] args) throws IOException {
        //17-18行 利用ServerSocketChannel的靜態方法open()產生serverChannel物件
        ServerSocketChannel serverChannel =
                ServerSocketChannel.open();
        //得到ServerSocket物件ss
        ServerSocket ss = serverChannel.socket();
        //將伺服器通道傾聽的埠號綁定為9950
        ss.bind(new InetSocketAddress(9950));
        //通道設定為Non-blocking模式
        serverChannel.configureBlocking(false);
        //呼叫Selector類別的靜態方法open()產生selector物件
        Selector selector = Selector.open();
        //將伺服器通道的接收連線事件註冊至selector選擇器 以便日後用戶端連線時可以得到通知
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);
        //30-行 持續監視(傾聽)迴圈
        while (true) {
            //呼叫選擇器的select()方法 進行等待註冊事件的發生 當有事件發生時 會進行下一行的處理
            selector.select();
            //事件發生(可能是有客戶端連線) 利用選擇器的selectedKeys()方法取得所有事件的鍵值 並放入Set集合物件keys
            Set keys = selector.selectedKeys();
            //取得集合內的所有元素it
            Iterator it = keys.iterator();
            //利用while迴圈走訪Iterator集合內的每一個元素
            while (it.hasNext()) {
                //40-41行 得到元素內的SelectionKey物件key 再自集合中移除該元素
                SelectionKey key = (SelectionKey) it.next();
                it.remove();
                //43-50行 若key值是剛連上線的事件 則印出訊息並取得該連線的通道物件client 再註冊其資料寫入的事件至選擇器中
                if (key.isAcceptable()) {
                    System.out.println("client connected");
                    ServerSocketChannel server =
                            (ServerSocketChannel) key.channel();
                    SocketChannel client = server.accept();
                    client.configureBlocking(false);
                    SelectionKey clientKey =
                            client.register(selector, SelectionKey.OP_WRITE);
                    //52-59行 若是資料寫入事件發生 即取得通道物件client 並以緩衝區方式 送出"ABC"字串至用戶端 最後關閉連線
                } else if (key.isWritable()) {
                    SocketChannel client = (SocketChannel) key.channel();
                    ByteBuffer buff = ByteBuffer.allocate(10);
                    buff.put("ABC".getBytes());
                    buff.flip();
                    client.write(buff);
                    client.close();
                }
            }
        }
    }
}
//執行本類別後 會持續傾聽9950埠號 接著使用Windows系統內附的telnet程式即可測試本伺服器運作
//開啟[/開始/程式集/附屬應用程式/命令提示字元] 輸入
//telnet localhost 9950 即可收到伺服器回應的"ABC"字串