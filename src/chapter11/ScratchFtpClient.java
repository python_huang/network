package chapter11;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
//11-26
public class ScratchFtpClient {
    //10-13行 定義本類別屬性
    String host;
    int port;
    BufferedReader in;
    PrintWriter out;
    //15-18行 類別的建構子 傳入主機名稱(或IP位址)與埠號兩個參數
    public ScratchFtpClient(String host, int port) {
        this.host = host;
        this.port = port;
    }
    //20-26行 呼叫此方法後立即連線至遠端FTP伺服器 並取得輸入資料流物件in與輸出資料流物件out
    public void connect() throws IOException,
            UnknownHostException {
        Socket socket = new Socket(host, port);
        in = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream());
    }
    //28-38行 主要是讀取伺服器連線後的歡迎訊息
    public void getWelcome() {
        try {
            String line;
            do {
                line = in.readLine();
                System.out.println(line);
            } while (line.charAt(3) != ' ');
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //40-43行 以FTP協定的USER與PASS指令傳送帳號與密碼 目前只有匿名登入
    public void login() {
        sendCommand("USER anonymous");
        sendCommand("PASS aa@com.tw");
    }
    //44-63行 送出指令至伺服器的方法 傳送指令至FTP伺服器端 並讀取伺服器端的回應訊息
    //並解析回應訊息中的回應碼 回傳給呼叫方一個回應碼
    public String sendCommand(String cmd) {
        String reply = null;
        //傳送指令
        out.println(cmd);
        out.flush();
        //取得伺服器的回應
        String line = null;
        try {
            do {
                line = in.readLine();
                System.out.println(line);
            } while (line.charAt(3) != ' ');
        } catch (IOException e) {
            e.printStackTrace();
        }
        reply = line;
        return reply;
    }
    //66-88行 專為PASSIVE被動式傳輸設計的讀取遠端檔案清單方法 傳送PASV指令至FTP伺服器 並解讀傳回資料通道的資訊後
    //建立資料連線並讀取FTP伺服器傳來的檔案清單
    public void pasvList() throws IOException,
        UnknownHostException{
        //得到如"140,117,11,7,29,5"
        String reply = sendCommand("PASV");
        String info = reply.substring(
                reply.indexOf("(") +1, reply.indexOf(")"));
        String tks[] = info.split(",");
        String ip = tks[0]+"." + tks[1] + "." + tks[2]
                + "." + tks[3];
        int p1 = Integer.parseInt(tks[4]);
        int p2 = Integer.parseInt(tks[5]);
        int port = p1 * 256 + p2;
        Socket dataSocket = new Socket(ip, port);
        sendCommand("LIST");
        InputStream dataIn = dataSocket.getInputStream();
        int data = dataIn.read();
        while (data != -1) {
            System.out.print((char) data);
            data = dataIn.read();
        }
        dataIn.close();
        dataSocket.close();
    }
    //90-103行 本類別的main方法 主要測試本類別與相關方法
    public static void main(String[] args) {
        ScratchFtpClient client = new ScratchFtpClient(
                "ftp.nsysu.edu.tw", 21);
        try {
            client.connect();
            client.getWelcome();
            client.login();
            client.pasvList();
        } catch (UnknownHostException e) {
            System.out.println("主機解析錯誤");
        } catch (IOException e) {
            System.out.println("資料輸出入錯誤");
        }
    }

}
