package chapter11;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import sun.net.ftp.FtpClient;
import sun.net.ftp.FtpProtocolException;

public class FTPUtil {
    /**
     * 連接ftp服務器 JDK 1.7
     *
     * @param url
     * @param port
     * @param username
     * @param password
     * @return FtpClient
     * @throws FtpProtocolException
     * @throws IOException
     */
    public static FtpClient connectFTP(String url, int port, String username,
                                       String password) { // 創建ftp
        FtpClient ftp = null;
        try {
            // 創建地址
            SocketAddress addr = new InetSocketAddress(url, port);
            // 連接
            ftp = FtpClient.create();
            ftp.connect(addr);
            // 登入
            ftp.login(username, password.toCharArray());
            ftp.setBinaryType();
            System.out.println(ftp.getWelcomeMsg());
        } catch (FtpProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ftp;
    }

    /**
     * 切換目錄
     *
     * @param ftp
     * @param path
     */
    public static void changeDirectory(FtpClient ftp, String path) {
        try {
            ftp.changeDirectory(path);
            System.out.println(ftp.getWorkingDirectory());
        } catch (FtpProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 關閉ftp
     *
     * @param ftp
     */
    public static void disconnectFTP(FtpClient ftp) {
        try {
            ftp.close();
            System.out.println("disconnect success!!");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 上傳文件
     *
     * @param localFile
     * @param ftpFile
     * @param ftp
     */
    public static void upload(String localFile, String ftpFile, FtpClient ftp) {
        OutputStream os = null;
        FileInputStream fis = null;
        try {
            // 將ftp文件加入輸出流中 輸出到ftp上
            os = ftp.putFileStream(ftpFile);
            File file = new File(localFile);
            // 創建一個緩衝區
            fis = new FileInputStream(file);
            byte[] bytes = new byte[1024];
            int c;
            while ((c = fis.read(bytes)) != -1) {
                os.write(bytes, 0, c);
            }
            System.out.println("upload success!!");
        } catch (FtpProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                if (os != null)
                    os.close();
                if (fis != null)
                    fis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * 文件下載
     *
     * @param localFile
     * @param ftpFile
     * @param ftp
     */
    public static void download(String localFile, String ftpFile, FtpClient ftp) {
        InputStream is = null;
        FileOutputStream fos = null;
        try {
            // 獲取ftp上的文件
            is = ftp.getFileStream(ftpFile);
            File file = new File(localFile);
            byte[] bytes = new byte[1024];
            int i;
            fos = new FileOutputStream(file);
            while ((i = is.read(bytes)) != -1) {
                fos.write(bytes, 0, i);
            }
            System.out.println("download success!!");
        } catch (FtpProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                if (fos != null)
                    fos.close();
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}