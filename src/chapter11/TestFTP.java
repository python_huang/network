package chapter11;


import java.net.InetAddress;
import java.net.UnknownHostException;
import sun.net.ftp.FtpClient;

public class TestFTP {
    public static void main(String[] args) {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            System.out.println(addr.getHostAddress());
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String ip = "10.10.10.10";
        int port = 21;
        String username = "root";
        String password = "root";
        String path = "/home";
        // 连接ftp
        FtpClient ftp = FTPUtil.connectFTP(ip, port, username, password);
        System.out.println(ftp.getWelcomeMsg());
        // 切换目录
        FTPUtil.changeDirectory(ftp, path);
        System.out.println("-----上傳----");
        FTPUtil.upload("D:aaa.txt", "/home/aaa.txt", ftp);
        System.out.println("-----下載----");
        FTPUtil.download("D:aaa.txt", "/home/aaa.txt", ftp);
    }
}