package chapterA;

import javax.swing.*;
import java.awt.*;

//A-23
//改為繼承javax.swing.JFrane
public class EditorFrameSwing extends JFrame {
    Button bOpen = new Button("Open");
    Button bSave = new Button("Save");
    Button bExit = new Button("Exit");
    TextArea ta = new TextArea();
    public EditorFrameSwing() {
        //取得JFrame內的子容器ContentPane 並以c物件存放
        Container c = getContentPane();
        //利用setDefaultCloseOperation方法設定當關閉視窗右上方的關閉鈕時 本類別也隨之終止程式運行
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300,250);
        Panel p = new Panel();
        p.add(bOpen);
        p.add(bSave);
        p.add(bExit);
        //24-25行 將p面板與ta輸入區塊加入JFrame的子容器c中
        c.add(p, BorderLayout.NORTH);
        c.add(p, BorderLayout.CENTER);
        setVisible(true);
    }
    public static void main(String[] args) {
        new EditorFrameSwing();
    }
}
