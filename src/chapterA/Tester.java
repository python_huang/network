package chapterA;

import javax.swing.*;

//A-5
public class Tester {
    public static void main(String[] args) {
        //產生Frame物件
        JFrame fm = new JFrame("測試用視窗程式");
        //設定Frame的大小
        fm.setSize(280,200);
        //使其顯示於畫面上
        fm.setVisible(true);
    }
}
