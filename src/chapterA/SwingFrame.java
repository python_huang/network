package chapterA;

import javax.swing.*;
import java.awt.*;

//A-25
public class SwingFrame extends JFrame {
    JTextArea ta = new JTextArea(3, 10);
    JButton b = new JButton("播放");
    public SwingFrame() {
        Container c = getContentPane();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300,100);
        Panel p = new Panel();
        p.add(b);
        c.add(p, BorderLayout.NORTH);
        setVisible(true);
    }
    public static void main (String[] args) {
        new SwingFrame();
    }
}
