package chapter7;

//7-19
//取消thunder()方法 將其工作移至run()方法中 並加入同步區塊的設計
public class Wizard2 extends Thread {
    public void run() {
        synchronized (this) {
            System.out.println("THUNDER!!");
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("END");
        }
    }

    public static void main (String[] args) {
        Wizard2 wizard = new Wizard2();
        Thread thr1 = new Thread(wizard);
        thr1.start();
        Thread thr2 = new Thread(wizard);
        thr2.start();
    }
}
