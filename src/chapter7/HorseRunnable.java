package chapter7;

//7-9
//有時一個已設計好的類別若已經繼承了其他父類別 而無法再繼承Thread類別時
//則可以實作[Runnable介面]間接達成執行緒的設計
//實作[implements]介面時 必須也要實作該介面的所有方法
//Runnable介面的唯一方法就是run()方法
public class HorseRunnable implements Runnable {
    public void run() {
        int h =0;
        for (int i = 0; i < 5000; i++) {
            h++;
            System.out.println(h);
        }
    }
}
