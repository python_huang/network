package chapter7;
import java.util.Vector;
//7-14
public class RankHorse extends Thread {
    //定義了成員rank
    Vector<RankHorse> rank;
    //新增了建構子 以接收Racing4所傳來的rank物件
    public RankHorse(Vector<RankHorse> rank) {
        this.rank = rank;
    }

    //覆寫Thread方法run()
    public void run() {
        try {
            sleep(2000);
            System.out.println(getName()+"到達終點");
            //放進rank集合中
            rank.add(this);
        } catch (InterruptedException e) {
            System.out.println(getName()+"被中斷了");
        }
    }
}
