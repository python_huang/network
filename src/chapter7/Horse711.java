package chapter7;

//7-11
//執行一次暫停2秒
//sleep方法使用時必須以try...catch處理被意外中斷的例外InterruptedException
public class Horse711 extends Thread {
    //覆寫Thread方法run()
    public void run() {
        try {
            sleep(2000);
            System.out.println(getName()+"到達終點");
        } catch (InterruptedException e) {
            System.out.println(getName()+"被中斷了");
        }
    }

}
