package chapter7;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

//7-24
//執行時傳遞一字串(以跳行字元為結尾)至伺服器端
//停頓5秒後 再開始接收伺服器端傳來的回應字串 停頓的原因是要在第一次執行EchoClient後
//再執行一次EchoClient連線至EchoServer 模擬並判別伺服器是否能夠服務多個客戶端程式的要求
public class EchoClient {
    public static void main(String[] args) {
        try {
            //產生連線至本機[localhost]埠號3333的連線物件s
            Socket s = new Socket("localhost", 3333);
            //分別得到輸入與輸出資料流物件
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            PrintWriter out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
            //利用輸出物件out送出"Hello"字串
            out.println("Hello");
            out.flush();
            System.out.println("已送出Hello");
            //停頓5秒 可依需求加長時間
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //利用in物件 讀取伺服器EchoServer送來的回音字串 並印出
            String rec = in.readLine();
            System.out.println("伺服器傳來:" + rec);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
