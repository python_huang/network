package chapter7;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

//7-21
//新增一個EchoServer類別 在其main方法中 建立ServerSocket物件 開始傾聽本機埠號3333
//EchoServer能不斷提供來連線的客戶端以達到多執行緒伺服器的功能
//因此使用while迴圈 持續接收客戶端連線 並將連線後得到的Socket物件socket傳遞給
//後續為每個客戶端服務的EchoThread類別 EchoThread類別將會處理相關的通訊協定
public class EchoServer7 {
    public static void main (String[] args) {
        try {
            ServerSocket server = new ServerSocket(3333);
            while (true){
                System.out.println("接受連線中");
                Socket socket = server.accept();
                //交由後續設計的Echo處理執行緒
                EchoThread echo = new EchoThread(socket);
                echo.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("伺服器窗口發生錯誤");
        }
    }
}
