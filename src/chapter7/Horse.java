package chapter7;

//7-6
//新增一個類別並繼承Thread 再覆寫Thread的run()方法 將要執行的工作程式碼放在run()方法內
//呼叫其方法start() 即可啟動該執行緒
public class Horse extends Thread {
    //覆寫Thread方法run()
    public void run() {
        //由1跑到5000
        int h = 0;
        for (int i = 0; i < 5000; i++) {
            h++;
            System.out.println(getName()+":"+h);
        }
    }
}
