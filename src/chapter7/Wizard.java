package chapter7;
//7-17
//同一時間只能有1名法師放出雷電 場上有2名法師
//同步方法(synchronized method)被限定只能有一個物件單獨使用它
//另一個執行緒就必須等待前一個執行完畢後才能取得執行權
public class Wizard extends Thread {
    public void run() {
        thunder();
    }

    public synchronized void thunder() {
        System.out.println("THUNDER!!");
        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("END");
    }

    public static void main (String[] args) {
        Wizard wizard = new Wizard();
        Thread thr1 = new Thread(wizard);
        thr1.start();
        Thread thr2 = new Thread(wizard);
        thr2.start();
    }
}
