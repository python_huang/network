package chapter7;

//7-5
//同一個執行緒 一定h1先到5000
public class RacingNG {
    public static void main(String[] args) {
        int h1 = 0;
        int h2 = 0;
        for (int i = 0; i < 5000; i++) {
            h1++;
            h2++;
            System.out.println("H1:"+h1);
            System.out.println("H2:"+h2);
        }
    }
}
