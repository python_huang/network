package chapter7;

import java.util.Vector;
//7-13
public class Racing4 {
    public static void main (String[] args) {
        //新增一個Vector物件rank 以[一般化]限定rank集合裡只能放置RankHorse物件
        Vector<RankHorse> rank = new Vector<RankHorse>();
        //第10-12利用RankHorse的建構子將rank物件傳遞給執行緒類別RankHorse內部使用
        RankHorse h1 = new RankHorse(rank);
        RankHorse h2 = new RankHorse(rank);
        RankHorse h3 = new RankHorse(rank);
        h1.setName("h1");
        h2.setName("h2");
        h3.setName("h3");
        h1.start();
        h2.start();
        h3.start();
        try {
            h1.join();
            h2.join();
            h3.join();
        } catch (InterruptedException e) {
            System.out.println("執行緒被中斷");
        }
        //依序印出rank集合內的所有物件(三匹馬)
        System.out.println(rank);
        System.out.println("main執行緒結束");
    }
}
