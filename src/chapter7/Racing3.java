package chapter7;

//7-10
//main方法專心處理產生執行緒與計算賽馬名次等工作
//三匹馬皆以Horse執行緒執行
public class Racing3 {
    public static void main(String[] args) {
        Horse711 h1 = new Horse711();
        Horse711 h2 = new Horse711();
        Horse711 h3 = new Horse711();
        h1.setName("h1");
        h2.setName("h2");
        h3.setName("h3");
        h1.start();
        h2.start();
        h3.start();
        System.out.println("main執行緒結束");
    }
}
