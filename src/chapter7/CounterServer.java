package chapter7;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

//7-28
//除伺服器類別CounterServer負責持續傾聽3335埠外
//再設計一個執行緒AddThread內部類別 負責讀檔與寫檔工作
public class CounterServer {
    String fileName = "counter.txt";
    //開始傾聽3335埠號的方法
    public void listen() {
        File file = new File(fileName);
        checkFile(file);
        try {
            ServerSocket server = new ServerSocket(3335);
            //持續傾聽用戶端的連線 一有連線產生則立刻產生執行緒AddThread 並啟動執行緒
            while (true) {
                System.out.println("接受連線中");
                Socket socket = server.accept();
                //交由後續設計的AddThread處理執行緒
                AddThread add = new AddThread(file);
                add.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("伺服器輸出入發生錯誤");
        }
    }
    //設計一個初次啟動時檢查檔案是否存在的方法checkFile
    //當counter.txt不存在時 產生此檔案內的初始計數值0
    public void checkFile (File file) {
        if (!file.exists()) {
            try {
                FileWriter out = new FileWriter(file, false);
                out.write("0\n");
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //內部類別:繼承Thread的執行緒類別 主要處理讀檔與寫入累加數字
    //覆寫父類別Thread的方法run() 並將工作設計於run()方法中
    class AddThread extends Thread {
        File file;
        public AddThread(File file) {
            this.file = file;
        }
        public void run() {
            //synchronized(file)區塊限定同時間只能允許一個執行緒存取計數檔
            synchronized (file) {
                System.out.println(getName() + "存取檔案中");
                BufferedReader in = null;
                FileWriter out = null;
                try {
                    in = new BufferedReader(new FileReader(file));
                    int n = Integer.parseInt(in.readLine());
                    in.close();
                    n++;
                    out = new FileWriter(file, false);
                    out.write(String.valueOf(n) + "\n");
                    out.flush();
                    out.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(getName() + "結束存取");
            }
        }
    }

    public static void main(String[] args) {
        CounterServer counter = new CounterServer();
        counter.listen();
    }
}