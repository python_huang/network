package chapter7;

//7-12
//Thread類別的方法join() 可以用來等待該執行緒完成
//必須以try...catch處理被意外中斷的例外InterruptrdException
public class Racing3_2 {
    public static void main (String[] args) {
        Horse711 h1 = new Horse711();
        Horse711 h2 = new Horse711();
        Horse711 h3 = new Horse711();
        h1.setName("h1");
        h2.setName("h2");
        h3.setName("h3");
        h1.start();
        h2.start();
        h3.start();
        try {
            h1.join();
            h2.join();
            h3.join(); //等待三匹馬執行結束
        } catch (InterruptedException e) {
            System.out.println("執行緒被中斷");
        }
        System.out.println("main執行緒結束");
    }
}
