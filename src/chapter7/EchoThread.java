package chapter7;

import java.io.*;
import java.net.Socket;

//7-23
//執行後接收客戶端的一行字串 伺服器在字串的頭尾各加上[*]星號後 回送給客戶端一行迴音資料
//由於run()方法內需要接收與送出資料 可以設計一建構子 接收java.net.Socket 可得到Socket後
//再取得InputStream與OutputStream物件 為了方便處理整行字串 此處將InputStream與OutputStream轉為
//BufferedReader與PrintWriter來處理 此制定的Echo通訊協定需求為:
//建立連線:
//客戶端送出一行最後含跳行自元的字串至伺服器:
//伺服器將收到的字串前後加上星號 並回送給客戶端:
//斷線。
public class EchoThread extends Thread {
    BufferedReader in;
    PrintWriter out;
    public EchoThread(Socket ss) throws IOException{
        in = new BufferedReader(new InputStreamReader(ss.getInputStream()));
        out = new PrintWriter(new OutputStreamWriter(ss.getOutputStream()));
    }
    public void run() {
        try {
            String str = in.readLine();
            System.out.println("EchoServer收到:"+str);
            str = "*"+str+"*";
            out.println(str);
            out.flush();
            out.close();
            in.close();
        } catch (IOException e) {
            System.out.println("發生傳輸例外");
        }
    }
}
