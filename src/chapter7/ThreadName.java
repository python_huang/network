package chapter7;
//7-4
public class ThreadName {
    public static void main(String[] args) {
        //得到目前的執行緒物件
        Thread thr = Thread.currentThread();
        System.out.println("目前執行緒名稱:"+thr.getName());
        //改變該執行緒的名稱
        thr.setName("DEMO");
        System.out.println("更改後的名稱:"+thr.getName());
    }
}
