package chapter6;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

//6-21
//執行時 等待封包到來
//UDPClient執行後 UDPServer會顯示收到封包的內容
public class UDPServer {
    public static void main (String[] args) throws IOException {
        byte[] buffer = new byte[10];
        DatagramPacket pkt = new DatagramPacket(buffer, 10);
        DatagramSocket ds = new DatagramSocket(9950);
        System.out.println("正在等待埠號:" + ds.getLocalPort());
        ds.receive(pkt);
        System.out.println("已收到UDP封包,封包內容:");
        for (int i = 0; i < buffer.length; i++)
            System.out.println(buffer[i]);
    }
}
