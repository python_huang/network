package chapter6;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

//6-20
//傳送一個內容是字串"ABC"的UDP封包至本機的9950埠
public class UDPClient {
    public static void main (String[] args) throws IOException {
        InetAddress addr = InetAddress.getLocalHost();
        String data = "ABC";
        byte[] buf = data.getBytes();
        DatagramPacket pkt = new DatagramPacket(buf, buf.length, addr, 9950);
        DatagramSocket ds = new DatagramSocket();
        ds.send(pkt);
    }
}
