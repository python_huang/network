package chapter6;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

//6-13 執行後 可使用Windows內建的telnet程式連線至localhost的8884埠號測試
//可看出計次的效果
public class CounterServer {
    public static int count = 0;

    public void startCount() {
        while (true) {
            try {
                ServerSocket ss = new ServerSocket(8884);
                Socket socket = ss.accept();
                count++;
                System.out.println("第" + count + "個客戶連線成功");
                OutputStream rawOut = socket.getOutputStream();
                PrintWriter out = new PrintWriter(rawOut);
                out.println("您是第" + count + "個客戶端");
                out.flush();
                out.close();
                socket.close();
                ss.close();
            } catch (IOException e) {
                System.out.println("輸出入錯誤");
            }
        }
    }

    public static void main(String[] args) {
        CounterServer cserver = new CounterServer();
        cserver.startCount();
    }
}
