package chapter6;

import java.io.IOException;
import java.net.ServerSocket;
//執行時 會由埠號1到65535開始建立ServerSocket
//如果該埠號已有程式占用 則建立物件時會拋出IOException
//再以catch區塊印出該埠號資訊
//6-9
public class PortScanner {
    public void scan() {
        for (int i = 1; i < 65535; i++) {
            try {
                ServerSocket ss = new ServerSocket(i);
            } catch (IOException e) {
                System.out.println("port "+ i + "被占用中");
            }
        }
    }

    public static void main(String[] args) {
        PortScanner pserver = new PortScanner();
        pserver.scan();
    }

}